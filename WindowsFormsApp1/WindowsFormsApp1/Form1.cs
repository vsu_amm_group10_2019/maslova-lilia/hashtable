﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        HashTable hashTable = new HashTable();
        private string fileName = string.Empty;
        public Form1()
        {
            InitializeComponent();
            label1.Visible = false;
            textBox1.Visible = false;
            button1.Visible = false;
            button2.Visible = false;
        }

        private void загрузитьФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openfile = new OpenFileDialog();
            StreamReader openedFile;
            openfile.Filter = "All Files (*.*)|*.*";
            if (openfile.ShowDialog() == DialogResult.OK)
            {
                openedFile = new StreamReader(openfile.FileName, Encoding.UTF8);
                fileName = openfile.FileName;
                richTextBox1.Text = openedFile.ReadToEnd();
                openedFile.Close();
                LoadInfo(hashTable);
            }
        }

        private void LoadInfo(HashTable hashTable)
        {
            for (int i = 0; i < richTextBox1.Lines.Length; i+=4)
            {
                Str s = new Str();
                s.tabel = int.Parse(richTextBox1.Lines[i]);
                s.FIO = richTextBox1.Lines[i + 1];
                s.salary = int.Parse(richTextBox1.Lines[i + 2]);
                hashTable.add(s);
            }
            richTextBox1.AppendText("\n" + "\n");
            label1.Visible = true;
            textBox1.Visible = true;
            button1.Visible = true;
            button2.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int tabel_num = int.Parse(textBox1.Text);
            hashTable.getWordInfo(tabel_num);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool check = false;
            int tabel_num = int.Parse(textBox1.Text);
            if (hashTable.delete(tabel_num))
            {
                if (richTextBox1.Lines.Length < 4)
                {
                    richTextBox1.Clear();
                }
                else
                {
                    for (int i = 0; i < richTextBox1.Lines.Length && !check; i += 4)
                    {
                        if (richTextBox1.Lines[i] == tabel_num.ToString())
                        {
                            richTextBox1.Text = richTextBox1.Text.Remove(richTextBox1.GetFirstCharIndexFromLine(i), richTextBox1.Lines[i].Length + 1);
                            richTextBox1.Text = richTextBox1.Text.Remove(richTextBox1.GetFirstCharIndexFromLine(i), richTextBox1.Lines[i].Length + 1);
                            richTextBox1.Text = richTextBox1.Text.Remove(richTextBox1.GetFirstCharIndexFromLine(i), richTextBox1.Lines[i].Length + 1);
                            richTextBox1.Text = richTextBox1.Text.Remove(richTextBox1.GetFirstCharIndexFromLine(i), richTextBox1.Lines[i].Length + 1);
                            check = true;
                        }
                    }
                }
            }
            richTextBox1.Refresh();
        }

        private void добавитьЭлементToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 dialog = new Form2();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Str s = dialog.Data;
                hashTable.add(s);
                richTextBox1.AppendText(s.tabel + "\n" + s.FIO + "\n" + s.salary + "\n" + "\n");
            }
        }
    }
}
