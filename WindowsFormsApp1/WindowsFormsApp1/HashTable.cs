﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Str
    {
        public int tabel;
        public string FIO;
        public int salary;
        public int deleted = -1;

        public Str(int _tabel, string _FIO, int _salary)
        {
            tabel = _tabel;
            FIO = _FIO;
            salary = _salary;
        }
        public Str()
        {
            tabel = -1;
            FIO = "";
            salary = -1;
        }
    };


    class HashTable
    {
        const int MAX_LENGTH = 64;
        public Str[] hashArray = new Str [MAX_LENGTH];

        public HashTable()
        {
            for (int i = 0; i < MAX_LENGTH; i++)
            {
                hashArray[i] = new Str();
            }
        }
        public int Hash(int tabel_num)
        {
            int hash = 0;
            while (tabel_num > 0)
            {
                hash += tabel_num % 10;
                tabel_num /= 10;
            }
            hash %= MAX_LENGTH;
            return hash;
        }
        public void add(Str s)
        {
            int hash = Hash(s.tabel);
            /*if (hashArray[hash] == null)
            {
                hashArray[hash] = new Str();
            }*/
            if (Math.Abs(hashArray[hash].deleted) == 1)
            {
                hashArray[hash].tabel = s.tabel;
                hashArray[hash].FIO = s.FIO;
                hashArray[hash].salary = s.salary;
                hashArray[hash].deleted = 0;
            }
            else
            {
                int i = hash+1;
                bool flag = false;
                if (i == MAX_LENGTH)
                {
                    i = 0;
                }
                while (!flag && i != hash)
                {
                    if (Math.Abs(hashArray[i].deleted) == 1)
                    {

                        hashArray[i].tabel = s.tabel;
                        hashArray[i].FIO = s.FIO;
                        hashArray[i].salary = s.salary;
                        hashArray[i].deleted = 0;
                        flag = true;
                    }
                    i++;
                    if (i == MAX_LENGTH)
                    {
                        i = 0;
                    }
                }
            }
        }
        public void getWordInfo(int tabel_num)
        {
            int hash = Hash(tabel_num);
            if (hashArray[hash].tabel == tabel_num && hashArray[hash].deleted == 0)
            {
                MessageBox.Show(hashArray[hash].tabel.ToString() + "\n" + hashArray[hash].FIO + "\n" + hashArray[hash].salary.ToString(), "Вывод", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (hashArray[hash].tabel == tabel_num && hashArray[hash].deleted == 1)
            {
                MessageBox.Show("Запись удалена", "Вывод", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (hashArray[hash].deleted == -1)
            {
                MessageBox.Show("Ячейка свободна", "Вывод", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                int i = hash + 1;
                bool flag = false;
                if (i == MAX_LENGTH)
                {
                    i = 0;
                }
                while (!flag && i != hash)
                {
                    if (hashArray[i].tabel == tabel_num && hashArray[i].deleted == 0)
                    {
                        MessageBox.Show(hashArray[i].tabel.ToString() + "\n" + hashArray[i].FIO + "\n" + hashArray[i].salary.ToString(), "Вывод", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        flag = true;
                    }
                    else if (hashArray[i].tabel == tabel_num && hashArray[i].deleted == 1)
                    {
                        MessageBox.Show("Запись удалена", "Вывод", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    i++;
                    if (i == MAX_LENGTH)
                    {
                        i = 0;
                    }
                }
            }
        }

        public bool delete(int tabel_num)
        {
            int hash = Hash(tabel_num);
            bool flag = false;
            if (hashArray[hash].tabel == tabel_num)
            {
                hashArray[hash].deleted = 1;
                flag = true;
            }
            else
            {
                int i = hash + 1;
                if (i == MAX_LENGTH)
                {
                    i = 0;
                }
                while (!flag && i != hash)
                {
                    if (hashArray[i].tabel == tabel_num)
                    {
                        hashArray[i].deleted = 1;
                        flag = true;
                    }
                    i++;
                    if (i == MAX_LENGTH)
                    {
                        i = 0;
                    }
                }
            }
            return flag;
        }
    }
}